import React, { Component } from 'react';
import { Text, View, StyleSheet, Image} from 'react-native';

export default class Logo extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image style={{width:150,height:150}}
                    source={require('../images/mars.png')}/>
                <Text style={styles.logoText}>Welcome</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {       
        flexGrow:1,
        justifyContent:'flex-end',
        alignItems:'center'
    },
    logoText: {
        fontSize:20,
        marginVertical:15,
        color:'#000000'
    }
});